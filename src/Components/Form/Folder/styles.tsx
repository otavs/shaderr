import styled from 'styled-components'
import { IoIosArrowForward } from 'react-icons/io'

export const Main = styled.div``

export const HeaderDiv = styled.div<{ isOpen: boolean }>`
  display: flex;
  align-items: center;
  padding: 4px 0;
  /* background-color: ${({ isOpen }) => (isOpen ? '#adadad' : '')}; */
  &:hover {
    background-color: #adadad;
    cursor: pointer;
  }
`

export const Arrow = styled(IoIosArrowForward)<{ rotated: boolean }>`
  transform: ${({ rotated }) => (rotated ? 'rotate(90deg)' : '')};
`

export const Content = styled.div<{ hidden: boolean }>`
  display: ${({hidden}) => hidden ? 'none' : ''}
`
