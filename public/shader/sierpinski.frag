#version 300 es

precision highp float;

uniform vec2 u_resolution;
uniform float u_time;
uniform vec2 u_mouse;
uniform float u_scroll;
uniform vec3 u_position;
uniform vec3 u_rotation;
uniform vec3 u_direction;

out vec4 fragColor;

// #extension GL_GOOGLE_include_directive : enable
// #include "./myFunctions.glsl"

uniform int maxRayTraceSteps;
uniform float maxRayTraceDist;
uniform float minRayTraceSurfaceDist;

mat3 rotateX(float theta) {
    float c = cos(theta);
    float s = sin(theta);
    return mat3(
        vec3(1, 0, 0),
        vec3(0, c, -s),
        vec3(0, s, c)
    );
}

// Rotation matrix around the Y axis.
mat3 rotateY(float theta) {
    float c = cos(theta);
    float s = sin(theta);
    return mat3(
        vec3(c, 0, s),
        vec3(0, 1, 0),
        vec3(-s, 0, c)
    );
}

// Rotation matrix around the Z axis.
mat3 rotateZ(float theta) {
    float c = cos(theta);
    float s = sin(theta);
    return mat3(
        vec3(c, -s, 0),
        vec3(s, c, 0),
        vec3(0, 0, 1)
    );
}

mat3 identity = mat3(1, 0, 0, 0, 1, 0, 0, 0, 1);

float sdSphere(vec3 p, vec3 center, float radius) {
  return length(center - p) - radius;
}

float sdHorizontalPlane(vec3 p, float y) { return p.y - y; }

float sdHorizontalPlane2(vec3 p, float y) { return y - p.y; }

float sdBoxFrame( vec3 p, vec3 b, float e ) {
       p = abs(p  )-b;
  vec3 q = abs(p+e)-e;
  return min(min(
      length(max(vec3(p.x,q.y,q.z),0.0))+min(max(p.x,max(q.y,q.z)),0.0),
      length(max(vec3(q.x,p.y,q.z),0.0))+min(max(q.x,max(p.y,q.z)),0.0)),
      length(max(vec3(q.x,q.y,p.z),0.0))+min(max(q.x,max(q.y,p.z)),0.0));
}

float sdSierpinski(vec3 p) {
  float s = 7.;
	vec3 a1 = vec3(1,1,1) * s;
	vec3 a2 = vec3(-1,-1,1) * s;
	vec3 a3 = vec3(1,-1,-1) * s;
	vec3 a4 = vec3(-1,1,-1) * s;
	vec3 c;
	int n = 0;
	float dist, d;
  int Iterations = 10;
  float Scale = 2.;
	while (n < Iterations) {
		c = a1; dist = length(p-a1);
	  d = length(p-a2); if (d < dist) { c = a2; dist=d; }
		d = length(p-a3); if (d < dist) { c = a3; dist=d; }
		d = length(p-a4); if (d < dist) { c = a4; dist=d; }
		p = Scale*p-c*(Scale-1.0);
		n++;
	}

	return length(p) * pow(Scale, float(-n));
}

float sdSierpinski2(vec3 z) {
    int n = 0;
    int Iterations = 10;
    float Scale = 2., Offset = 2.;
    while (n < Iterations) {
       if(z.x+z.y<0.) z.xy = -z.yx; // fold 1
       if(z.x+z.z<0.) z.xz = -z.zx; // fold 2
       if(z.y+z.z<0.) z.zy = -z.yz; // fold 3	
       z = z*Scale - Offset*(Scale-1.0);
       n++;
    }
    return (length(z) ) * pow(Scale, -float(n));
}

float getDist(vec3 p) {
  float dist = maxRayTraceDist;

  bool rotations = true;

  mat3 rotation = rotations ? rotateX(.2 * u_time) * rotateZ(.3* u_time) : identity;
  dist = min(dist, sdSierpinski(rotation * p));

  // vec3 c = vec3(10, 10, 10);
  // vec3 q = mod(p+0.5*c,c)-0.5*c;
  // dist = min(dist, sdSierpinski(q));

  rotation = rotations ? rotateX(.3 * u_time) * rotateY(.2* u_time) : identity;
  dist = min(dist, sdSphere(p, vec3(0, 0, 0), 2.));
  dist = min(dist, sdBoxFrame(rotation * p, vec3(10, 10, 10), 1.));

  return dist;
}

float rayMarch(vec3 origin, vec3 dir) {
  float totalDist = 0.;
  for (int i = 0; i < maxRayTraceSteps; i++) {
    vec3 p = origin + totalDist * dir;
    float dist = getDist(p);
    totalDist += dist;
    if (totalDist > maxRayTraceDist || dist < minRayTraceSurfaceDist)
      break;
  }
  return totalDist;
}

vec3 getNormal(vec3 p) {
  float dist = getDist(p);
  vec2 e = vec2(.01, 0);
  vec3 normal =
      dist - vec3(getDist(p - e.xyy), getDist(p - e.yxy), getDist(p - e.yyx));
  return normalize(normal);
}

float getLight(vec3 p) {
  vec3 lightPos = u_position.xyz;
  vec3 pToLight = normalize(lightPos - p);
  vec3 normal = getNormal(p);
  float light = clamp(dot(normal, pToLight), 0., 1.);
  float q = rayMarch(p + 2. * minRayTraceSurfaceDist * normal, pToLight);
  if (q < length(lightPos - p)) {
    light *= .1;
  }
  return light;
}

vec3 rotateCamera(vec3 dir) {
  float a_x = -u_rotation.x;
  float a_y = -u_rotation.y;
  float a_z = 0.;

  mat3 Rz = mat3(cos(a_z), -sin(a_z), 0, sin(a_z), cos(a_z), 0, 0, 0, 1);
  mat3 Ry = mat3(cos(a_y), 0, sin(a_y), 0, 1, 0, -sin(a_y), 0, cos(a_y));
  mat3 Rx = mat3(1, 0, 0, 0, cos(a_x), -sin(a_x), 0, sin(a_x), cos(a_x));

  return Rz * Ry * Rx * dir;
}

void main() {
  vec2 uv = (2.0 * gl_FragCoord.xy - u_resolution.xy) / u_resolution.y;

  vec3 dir = normalize(vec3(uv, 2));
  dir = rotateCamera(dir);

  float dist = rayMarch(u_position, dir);
  vec3 p = u_position + dist * dir;
  float difuseLight = getLight(p);

  vec3 col = vec3(difuseLight);
  col = getNormal(p);
  fragColor = vec4(col, 1.0);
}