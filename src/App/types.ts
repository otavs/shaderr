import * as twgl from 'twgl.js'

export interface ShaderApp {
  vertFile: string
  fragFile: string
  uniforms?: Uniforms
  programInfo?: twgl.ProgramInfo
  mouseLock?: boolean
}

export interface ShaderFile {
  file: string
  shader: string
}

export type Uniform = UniformInt | UniformFloat | UniformVec2 | UniformVec3

export interface Uniforms {
  [name: string]: Uniform
}

export interface UniformInt {
  type: 'int'
  displayName: string
  default: number
  value?: number
  min?: number
  max?: number
}

export interface UniformFloat {
  type: 'float'
  displayName: string
  default: number
  value?: number
  min?: number
  max?: number
}

export interface UniformVec2 {
  type: 'vec2'
  displayName: string
  default: Vec2
  value?: Vec2
  min?: Vec2
  max?: Vec2
}

export interface UniformVec3 {
  type: 'vec3'
  displayName: string
  default: Vec3
  value?: Vec3
  min?: Vec3
  max?: Vec3
}

export type Vec2 = [number, number]
export type Vec3 = [number, number, number]