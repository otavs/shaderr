import * as twgl from 'twgl.js'
import { ShaderApp, Uniform } from './types'
import { Apps, Shaders } from './Apps'
import {
  mousePosition,
  scrollPosition,
  time,
  rotationSpeed,
  keyState,
  cameraDir,
  cameraPosition,
  cameraRotation,
  reset,
  update,
} from './controls'
import Stats from 'stats.js'
import { uniformsAtom } from 'State'
import { copy } from 'utils'

const scrollScaleFactor = 1.1

let gl: WebGL2RenderingContext
let currentApp: ShaderApp
let stats: Stats

export function setup(gl_: WebGL2RenderingContext) {
  gl = gl_

  Object.entries(Apps).forEach(([_, app]) => {
    app.programInfo = twgl.createProgramInfo(gl, [
      Shaders[app.vertFile],
      Shaders[app.fragFile],
    ])
    if (app.uniforms) {
      Object.entries(app.uniforms).forEach(([_, uniform]) => {
        uniform.value = copy(uniform.default)
      })
    }
  })

  stats = new Stats()
  stats.dom.style.left = ''
  stats.dom.style.right = '0'
  stats.showPanel(0)
  document.body.appendChild(stats.dom)
}

const onMouseMove = (e: MouseEvent) => {
  // or touchstart
  mousePosition.x = e.offsetX
  mousePosition.y = gl.canvas.height - e.offsetY
  if (document.pointerLockElement) {
    cameraRotation.y += e.movementX * rotationSpeed
    cameraRotation.x += e.movementY * rotationSpeed
  }
}

const onWheel = (e: WheelEvent) => {
  scrollPosition.y *= e.deltaY > 0 ? scrollScaleFactor : 1 / scrollScaleFactor
}

const onClick = (e: MouseEvent) => {
  if (currentApp.mouseLock && !document.pointerLockElement)
    gl.canvas.requestPointerLock()
}

const onKeyDown = (e: KeyboardEvent) => {
  keyState[e.key] = true
}

const onKeyUp = (e: KeyboardEvent) => {
  keyState[e.key] = false
}

let isRendering = false

function render() {
  stats.begin()
  twgl.resizeCanvasToDisplaySize(gl.canvas)
  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height)

  update()

  twgl.setUniforms(currentApp.programInfo!, {
    u_resolution: [gl.canvas.width, gl.canvas.height],
    u_time: (performance.now() - time.start) / 1000,
    u_mouse: [mousePosition.x, mousePosition.y],
    u_scroll: scrollPosition.y,
    u_position: [cameraPosition.x, cameraPosition.y, cameraPosition.z],
    u_rotation: [cameraRotation.x, cameraRotation.y, cameraRotation.z],
    u_direction: [cameraDir.x, cameraDir.y, cameraDir.z],
  })

  if (currentApp.uniforms) {
    const otherUniforms: any = {}
    for (const [name, uniform_] of Object.entries(currentApp.uniforms as any)) {
      const uniform = uniform_ as Uniform
      otherUniforms[name] = uniform.value
    }
  
    twgl.setUniforms(currentApp.programInfo!, otherUniforms)
  }

  gl.drawArrays(gl.TRIANGLE_FAN, 0, 3)

  stats.end()

  if (isRendering) requestAnimationFrame(render)
}

export function startRendering() {
  isRendering = true
  gl.canvas.addEventListener('mousemove', onMouseMove)
  gl.canvas.addEventListener('wheel', onWheel)
  gl.canvas.addEventListener('click', onClick)
  gl.canvas.addEventListener('keydown', onKeyDown)
  gl.canvas.addEventListener('keyup', onKeyUp)
  render()
}

export function stopRendering() {
  isRendering = false
}

export function setCurrentApp(appName: string) {
  currentApp = Apps[appName]
  reset()
  gl.useProgram(currentApp.programInfo!.program)
}
