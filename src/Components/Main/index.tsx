import { useState, useEffect, useRef } from 'react'
import { setup, setCurrentApp, startRendering, stopRendering } from 'App'
import { appNameAtom } from 'State'
import { Apps } from 'App/Apps'
import { TransparentDiv, Select, Canvas } from './styles'
import {
  scrollPosition,
  time,
  movementSpeed,
  cameraDir,
  cameraPosition,
} from 'App/controls'
import Form from 'Components/Form'
import { useAtom } from 'jotai'

function Main() {
  const [appName, setAppName] = useAtom(appNameAtom)
  const canvasRef = useRef<HTMLCanvasElement>(null)

  useEffect(init, [])

  useEffect(() => {
    setCurrentApp(appName)
  }, [appName])

  return (
    <>
      <TransparentDiv>
        <Select value={appName} onChange={(e) => setAppName(e.target.value)}>
          {Object.entries(Apps).map(([appName]) => (
            <option value={appName} key={appName}>
              {appName}
            </option>
          ))}
        </Select>
        <Form />
      </TransparentDiv>
      <Canvas ref={canvasRef} tabIndex={1} />
    </>
  )

  function init() {
    const gl = canvasRef.current?.getContext('webgl2')

    if (!gl) {
      alert('No WebGL :(')
      return
    }

    setup(gl)
    setCurrentApp(appName)
    startRendering()

    return () => {
      // clearInterval(interval)
      stopRendering()
    }
  }
}

export default Main
