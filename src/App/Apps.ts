import { ShaderApp, ShaderFile, Uniform, Uniforms } from './types'

export const Shaders: { [name: string]: string } = {}

const rayTraceUniforms: Uniforms = {
  maxRayTraceSteps: {
    displayName: 'Max Steps',
    type: 'int',
    default: 256,
    max: 256 * 2,
    min: 0,
  },
  maxRayTraceDist: {
    displayName: 'Max Dist',
    type: 'float',
    default: 256,
    max: 256 * 4,
    min: 0,
  },
  minRayTraceSurfaceDist: {
    displayName: 'Min Surface Dist',
    type: 'float',
    default: 0.01,
    max: 1,
    min: 0,
  },
}

export const Apps: { [name: string]: ShaderApp } = {
  'Hello World': {
    vertFile: 'index.vert',
    fragFile: 'helloWorld.frag',
  },
  HSV: {
    vertFile: 'index.vert',
    fragFile: 'hsv.frag',
  },
  // 'Ray Marcher Hello World': {
  //   vertFile: 'index.vert',
  //   fragFile: 'rayMarchHelloWorld.frag',
  // },
  'Ray Marcher Mouse Lock': {
    vertFile: 'index.vert',
    fragFile: 'rayMarchMouseLock.frag',
    mouseLock: true,
    uniforms: {
      ...rayTraceUniforms,
    },
  },
  Spheres: {
    vertFile: 'index.vert',
    fragFile: 'spheres.frag',
    mouseLock: true,
    uniforms: {
      // u_period3: {
      //   displayName: 'Period 3',
      //   type: 'vec3',
      //   default: [2, 2, 2],
      //   max: [10, 10, 10],
      //   min: [0, 0, 0]
      // },
      // u_period2: {
      //   displayName: 'Period 2',
      //   type: 'vec2',
      //   default: [2, 2],
      //   max: [10, 10],
      //   min: [0, 0]
      // },
      // u_period1: {
      //   displayName: 'Period 1',
      //   type: 'float',
      //   default: 1,
      //   max: 100,
      //   min: 0
      // },
      gap: {
        displayName: 'Gap',
        type: 'vec3',
        default: [20, 20, 20],
        max: [400, 400, 400],
        min: [0, 0, 0],
      },
      ...rayTraceUniforms,
    },
  },
  Sierpinski: {
    vertFile: 'index.vert',
    fragFile: 'sierpinski.frag',
    mouseLock: true,
    uniforms: {
      ...rayTraceUniforms,
    },
  },
}

export async function loadApps() {
  const shaderFiles = new Set<string>()
  Object.entries(Apps).forEach(([_, app]) => {
    shaderFiles.add(app.vertFile).add(app.fragFile)
  })

  const promises: Promise<ShaderFile>[] = []
  shaderFiles.forEach((file) => {
    promises.push(getShader(file))
  })

  const shaders = await Promise.all(promises)
  for (const { file, shader } of shaders) {
    Shaders[file] = shader
  }
}

async function getShader(name: string): Promise<ShaderFile> {
  return await fetch(`/shader/${name}`).then(async (res) => ({
    file: name,
    shader: await res.text(),
  }))
}
