#version 300 es

precision highp float;

uniform vec2 u_resolution;
uniform float u_time;
uniform vec2 u_mouse;
uniform float u_scroll;
uniform vec3 u_position;
uniform vec3 u_rotation;
uniform vec3 u_direction;

uniform vec3 gap;

out vec4 fragColor;

uniform int maxRayTraceSteps;
uniform float maxRayTraceDist;
uniform float minRayTraceSurfaceDist;

vec3 hsv2rgb(vec3 c) {
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float sdSphere(vec3 p, float radius) { return length(p) - radius; }

float sdBox(vec3 p, vec3 b) {
  vec3 q = abs(p) - b;
  return length(max(q, 0.0)) + min(max(q.x, max(q.y, q.z)), 0.0);
}

float hashwithoutsine13(vec3 p3) {
  p3 = fract(p3 * .1031);
  p3 += dot(p3, p3.yzx + 33.33);
  return fract((p3.x + p3.y) * p3.z);
}

float map(float value, float min1, float max1, float min2, float max2) {
  return min2 + (value - min1) * (max2 - min2) / (max1 - min1);
}

vec4 sdUnion(vec4 a, vec4 b) { return a.w < b.w ? a : b; }

vec4 repSphere(vec3 p, vec3 period, float seed) {
  vec3 halfPeriod = .5 * period;
  vec3 id = floor((p + halfPeriod) / period);

  float rand = hashwithoutsine13(id + seed);

  float radius = 1. / u_scroll;

  // if (hashwithoutsine13(id) < .95) {
  //   return vec4(0, 0, 0, length(period)/4.);
  // }

  vec3 q = mod(p + halfPeriod, period) - halfPeriod;

  // return vec4(rand, fract(rand * 41231.), fract(rand * 1422.),
  //             sdSphere(q, radius));

  return vec4(hsv2rgb(vec3(rand, 1, 1)), sdSphere(q, radius));
}

vec4 getDist(vec3 p) {
  vec4 dist = vec4(0, 0, 0, maxRayTraceDist);

  dist = sdUnion(dist, repSphere(p + 10., gap, 0.));

  return dist;
}

vec3 getNormal(vec3 p) {
  float dist = getDist(p).w;
  vec2 e = vec2(.01, 0);
  vec3 normal = dist - vec3(getDist(p - e.xyy).w, getDist(p - e.yxy).w,
                            getDist(p - e.yyx).w);
  return normalize(normal);
}

vec4 rayMarch(vec3 origin, vec3 dir) {
  float totalDist = 0.;
  vec4 dist;
  for (int i = 0; i < maxRayTraceSteps; i++) {
    vec3 p = origin + totalDist * dir;
    dist = getDist(p);
    totalDist += dist.w;
    if (totalDist > maxRayTraceDist || dist.w < minRayTraceSurfaceDist)
      break;
  }
  return vec4(dist.rgb, totalDist);
}

float getLight(vec3 p) {
  // vec3 lightPos = vec3(0, 4, 4);
  // lightPos.xz += vec2(sin(u_time), cos(u_time)) * 2.;
  vec3 lightPos = u_position;
  vec3 pToLight = normalize(lightPos - p);
  vec3 normal = getNormal(p);
  float light = clamp(dot(normal, pToLight), 0., 1.);
  float q = rayMarch(p + 2. * minRayTraceSurfaceDist * normal, pToLight).w;
  if (q < length(lightPos - p)) {
    light *= .1;
  }
  return light;
}

vec3 rotateCamera(vec3 dir) {
  float a_x = -u_rotation.x;
  float a_y = -u_rotation.y;
  float a_z = 0.;

  mat3 Rz = mat3(cos(a_z), -sin(a_z), 0, sin(a_z), cos(a_z), 0, 0, 0, 1);
  mat3 Ry = mat3(cos(a_y), 0, sin(a_y), 0, 1, 0, -sin(a_y), 0, cos(a_y));
  mat3 Rx = mat3(1, 0, 0, 0, cos(a_x), -sin(a_x), 0, sin(a_x), cos(a_x));

  return Rz * Ry * Rx * dir;
}

void main() {
  vec2 uv = (2.0 * gl_FragCoord.xy - u_resolution.xy) / u_resolution.y;

  vec3 dir = normalize(vec3(uv, 2.));
  dir = rotateCamera(dir);

  vec4 dist = rayMarch(u_position, dir);

  if (dist.w >= maxRayTraceDist) {
    fragColor = vec4(vec3(0.65, 0.98, 1) * 0., 1.);
    return;
  }

  vec3 p = u_position + dist.w * dir;
  float difuseLight = getLight(p);

  vec3 period = vec3(20.);
  vec3 halfPeriod = period / 2.;
  vec3 id = floor((p + halfPeriod) / period);
  float rand = hashwithoutsine13(id + 8.);
  // vec3 col = hsv2rgb(abs(getNormal(p)));

  fragColor = vec4(difuseLight * dist.rgb, 1.0);
}