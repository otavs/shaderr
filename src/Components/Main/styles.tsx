import styled from 'styled-components'

export const Canvas = styled.canvas`
  width: 100%;
  height: 100%;
  display: block;
`

export const Select = styled.select`
  background-color: rgba(0, 0, 0, 0);
`

export const TransparentDiv = styled.div`
  position: absolute;
  max-height: 100%;
  background-color: #ffffffc8;
  /* background-color: rgba(0, 0, 0, 0);
  overflow-y: auto;
  padding: 6px;
  filter: invert(1);
  mix-blend-mode: difference;
  color: grey; */
`
