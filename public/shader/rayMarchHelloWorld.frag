#version 300 es

precision highp float;

uniform vec2 u_resolution;
uniform float u_time;
uniform vec2 u_mouse;
uniform float u_scroll;

out vec4 fragColor;

uniform int maxRayTraceSteps;
uniform float maxRayTraceDist;
uniform float minRayTraceSurfaceDist;

float getDist(vec3 p) {
  vec3 sphereCenter = vec3(0, 1, 6);
  float sphereRadius = 1.;
  float sphereDist = length(p - sphereCenter) - sphereRadius;
  float planeDist = p.y;
  return min(sphereDist, planeDist);
}

float rayMarch(vec3 origin, vec3 dir) {
  float totalDist = 0.;
  for (int i = 0; i < maxRayTraceSteps; i++) {
    vec3 p = origin + totalDist * dir;
    float dist = getDist(p);
    totalDist += dist;
    if (totalDist > maxRayTraceDist || dist < minRayTraceSurfaceDist)
      break;
  }
  return totalDist;
}

vec3 getNormal(vec3 p) {
  float dist = getDist(p);
  vec2 e = vec2(.01, 0);
  vec3 normal =
      dist - vec3(getDist(p - e.xyy), getDist(p - e.yxy), getDist(p - e.yyx));
  return normalize(normal);
}

float getLight(vec3 p) {
  vec3 lightPos = vec3(0, 5, 6);
  lightPos.xz += vec2(sin(u_time), cos(u_time)) * 2.;
  vec3 pToLight = normalize(lightPos - p);
  vec3 normal = getNormal(p);
  float light = clamp(dot(normal, pToLight), 0., 1.);
  float q = rayMarch(p + 2. * minRayTraceSurfaceDist * normal, pToLight);
  if (q < length(lightPos - p)) {
    light *= .1;
  }
  return light;
}

void main() {
  vec2 uv = (2.0 * gl_FragCoord.xy - u_resolution.xy) / u_resolution.y;
  vec2 mouse = (2.0 * u_mouse - u_resolution.xy) / u_resolution.y;
  vec3 col = vec3(0);
  vec3 origin = vec3(0, 1, 0);
  vec3 dir = normalize(vec3(uv + mouse, 1));
  float dist = rayMarch(origin, dir);
  vec3 p = origin + dist * dir;
  float difuseLight = getLight(p);
  col = vec3(difuseLight);
  // col = getNormal(p);
  fragColor = vec4(col, 1.0);
}