#version 300 es

precision highp float;

uniform vec2 u_resolution;
uniform float u_time;
uniform vec2 u_mouse;
uniform float u_scroll;
uniform vec3 u_position;
uniform vec3 u_rotation;
uniform vec3 u_direction;

out vec4 fragColor;

uniform int maxRayTraceSteps;
uniform float maxRayTraceDist;
uniform float minRayTraceSurfaceDist;

vec3 hsv2rgb(vec3 c) {
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float sdSphere(vec3 p, float radius) { return length(p) - radius; }

float sdSphere2(vec3 p, float radius, float rand) {
  return length(p) - 2. / u_scroll;
}

float sdHorizontalPlane(vec3 p, float y) { return p.y - y; }

float sdHorizontalPlane2(vec3 p, float y) { return y - p.y; }

float mod289(float x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
vec4 mod289(vec4 x) { return x - floor(x * (1.0 / 289.0)) * 289.0; }
vec4 perm(vec4 x) { return mod289(((x * 34.0) + 1.0) * x); }

float noise(vec3 p) {
  vec3 a = floor(p);
  vec3 d = p - a;
  d = d * d * (3.0 - 2.0 * d);

  vec4 b = a.xxyy + vec4(0.0, 1.0, 0.0, 1.0);
  vec4 k1 = perm(b.xyxy);
  vec4 k2 = perm(k1.xyxy + b.zzww);

  vec4 c = k2 + a.zzzz;
  vec4 k3 = perm(c);
  vec4 k4 = perm(c + 1.0);

  vec4 o1 = fract(k3 * (1.0 / 41.0));
  vec4 o2 = fract(k4 * (1.0 / 41.0));

  vec4 o3 = o2 * d.z + o1 * (1.0 - d.z);
  vec2 o4 = o3.yw * d.x + o3.xz * (1.0 - d.x);

  return o4.y * d.y + o4.x * (1.0 - d.y);
}

// UE4's PseudoRandom function
// https://github.com/EpicGames/UnrealEngine/blob/release/Engine/Shaders/Private/Random.ush
float pseudo(vec2 v) {
  v = fract(v / 128.) * 128. + vec2(-64.340622, -72.465622);
  return fract(dot(v.xyx * v.xyy, vec3(20.390625, 60.703125, 2.4281209)));
}

float hashwithoutsine13(vec3 p3) {
  p3 = fract(p3 * .1031);
  p3 += dot(p3, p3.yzx + 33.33);
  return fract((p3.x + p3.y) * p3.z);
}

float repSphere(vec3 p, vec3 period, float radius) {
  vec3 id = floor((p + period / 2.) / period) * period;

  float rand = hashwithoutsine13(id);
  if (rand < .95) {
    return 1.;
  }
  vec3 q = mod(p + 0.5 * period, period) - 0.5 * period;
  return sdSphere2(q, radius, rand);
}

float getDist(vec3 p) {
  float dist = maxRayTraceDist;

  dist = min(dist,
             repSphere(vec3(0, 0, 4) - p + vec3(2.5), vec3(5.), 1. / u_scroll));
  // dist = min(dist, sdHorizontalPlane(p, -1.));
  // dist = min(dist, sdHorizontalPlane2(p, 7.));

  return dist;
}

float rayMarch(vec3 origin, vec3 dir) {
  float totalDist = 0.;
  for (int i = 0; i < maxRayTraceSteps; i++) {
    vec3 p = origin + totalDist * dir;
    float dist = getDist(p);
    totalDist += dist;
    if (totalDist > maxRayTraceDist || dist < minRayTraceSurfaceDist)
      break;
  }
  return totalDist;
}

vec3 getNormal(vec3 p) {
  float dist = getDist(p);
  vec2 e = vec2(.01, 0);
  vec3 normal =
      dist - vec3(getDist(p - e.xyy), getDist(p - e.yxy), getDist(p - e.yyx));
  return normalize(normal);
}

float getLight(vec3 p) {
  // vec3 lightPos = vec3(0, 4, 4);
  // lightPos.xz += vec2(sin(u_time), cos(u_time)) * 2.;
  vec3 lightPos = u_position;
  vec3 pToLight = normalize(lightPos - p);
  vec3 normal = getNormal(p);
  float light = clamp(dot(normal, pToLight), 0., 1.);
  float q = rayMarch(p + 2. * minRayTraceSurfaceDist * normal, pToLight);
  if (q < length(lightPos - p)) {
    light *= .1;
  }
  return light;
}

vec3 rotateCamera(vec3 dir) {
  float a_x = -u_rotation.x;
  float a_y = -u_rotation.y;
  float a_z = 0.;

  mat3 Rz = mat3(cos(a_z), -sin(a_z), 0, sin(a_z), cos(a_z), 0, 0, 0, 1);
  mat3 Ry = mat3(cos(a_y), 0, sin(a_y), 0, 1, 0, -sin(a_y), 0, cos(a_y));
  mat3 Rx = mat3(1, 0, 0, 0, cos(a_x), -sin(a_x), 0, sin(a_x), cos(a_x));

  return Rz * Ry * Rx * dir;
}

void main() {
  vec2 uv = (2.0 * gl_FragCoord.xy - u_resolution.xy) / u_resolution.y;

  vec3 dir = normalize(vec3(uv, 2));
  dir = rotateCamera(dir);

  float dist = rayMarch(u_position, dir);
  vec3 p = u_position + dist * dir;
  float difuseLight = getLight(p);

  // vec3 col = vec3(difuseLight);
  vec3 col = hsv2rgb(abs(getNormal(p)));
  fragColor = vec4(col, 1.0);
}