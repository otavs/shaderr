import { useState, useEffect, useRef } from 'react'
import { setup, setCurrentApp, startRendering, stopRendering } from 'App'
import { Apps } from 'App/Apps'
import { ShaderApp, UniformFloat, Uniforms, Vec3 } from 'App/types'
import {
  scrollPosition,
  time,
  movementSpeed,
  cameraDir,
  cameraPosition,
} from 'App/controls'
import { useAtom } from 'jotai'
import { appNameAtom, uniformsAtom } from 'State'
import { FormParams, Input, InputDiv, SelectorDiv } from './styles'
import { copy, isNumeric } from 'utils'
import Folder from './Folder'

function Form() {
  const [appName, setAppName] = useAtom(appNameAtom)
  const [uniforms, setUniforms] = useAtom(uniformsAtom)

  useEffect(() => {
    const uniformsModel = Apps[appName].uniforms
    if (!uniformsModel) {
      setUniforms({})
      return
    }
    const newUniforms: Uniforms = {}
    for (const name in uniformsModel) {
      newUniforms[name] = copy(uniformsModel[name])
      // todo: move to setup
      newUniforms[name].value = copy(uniformsModel[name].default)
    }
    setUniforms(newUniforms)
  }, [appName])

  const fields = Object.entries(uniforms).map(([name, uniform]) => {
    switch (uniform.type) {
      case 'int':
      case 'float': {
        const { displayName, type, min, max, value } = uniform
        return (
          <div key={name}>
            <InputDiv>
              {displayName}:
              <Input
                type="text"
                width={`${value}`.length}
                value={value}
                onChange={(e) => {
                  e.target.value = fixNumberInput(e.target.value)
                  if (!isNumeric(e.target.value)) return
                  Apps[appName].uniforms![name].value = +e.target.value
                  setUniforms((draft) => {
                    draft[name].value = +e.target.value
                  })
                }}
              />
            </InputDiv>
            <input
              type="range"
              min={min}
              max={max}
              value={value}
              step={type == 'int' ? 1 : 0.01}
              key={name}
              onChange={(e) => {
                e.target.value = fixNumberInput(e.target.value)
                if (!isNumeric(e.target.value)) return
                Apps[appName].uniforms![name].value = +e.target.value
                setUniforms((draft) => {
                  draft[name].value = +e.target.value
                })
              }}
            />
          </div>
        )
      }
      case 'vec2':
      case 'vec3': {
        const { displayName, min, max, value } = uniform
        return (
          <div key={name}>
            <InputDiv>
              {displayName}: [
              {value?.map((v, i) => (
                <>
                  {i > 0 ? ',' : ''}
                  <Input
                    type="text"
                    width={`${v}`.length}
                    value={v}
                    onChange={(e) => {
                      e.target.value = fixNumberInput(e.target.value)
                      if (!isNumeric(e.target.value)) return
                      const uniform = Apps[appName].uniforms![name]
                        .value as Vec3
                      uniform[i] = +e.target.value
                      setUniforms((draft) => {
                        const uniform = draft[name].value as Vec3
                        uniform[i] = +e.target.value
                      })
                    }}
                  />
                </>
              ))}
              ]
            </InputDiv>
            {value?.map((v, i) => (
              <SelectorDiv key={name + i}>
                <input
                  type="range"
                  min={min?.[i]}
                  max={max?.[i]}
                  value={v}
                  step={0.01}
                  onChange={(e) => {
                    e.target.value = fixNumberInput(e.target.value)
                    if (!isNumeric(e.target.value)) return
                    const uniform = Apps[appName].uniforms![name].value as Vec3
                    uniform[i] = +e.target.value
                    setUniforms((draft) => {
                      const uniform = draft[name].value as Vec3
                      uniform[i] = +e.target.value
                    })
                  }}
                />
              </SelectorDiv>
            ))}
          </div>
        )
      }
    }
  })

  const folder = <Folder>Hello World</Folder>

  return (
    <FormParams>
      {fields}
      {folder}
    </FormParams>
  )
}

function fixNumberInput(s: string): string {
  if (s == undefined || s == null) return s
  s = s.trim().replace(/^0+/, '')
  const sign = s[0] == '-' ? '-' : ''
  let res = s.replace(/^-?0*/, '')
  if (res == '') res = '0'
  return `${sign}${res}`
}

export default Form
