export function copy(obj: any) {
  return JSON.parse(JSON.stringify(obj))
}

export function isNumeric(str: string): boolean {
  return !isNaN(str as any) && !isNaN(parseFloat(str))
}