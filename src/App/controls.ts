export let movementSpeed = 0.1
export const rotationSpeed = 0.001

export const keyState: { [name: string]: boolean } = {}

export const mousePosition = {
  x: 0,
  y: 0,
}

export const scrollPosition = {
  x: 0,
  y: 0,
}

export const cameraPosition = {
  x: 0,
  y: 0,
  z: 0,
}

export const cameraRotation = {
  x: 0,
  y: 0,
  z: 0,
}

export const cameraDir = {
  x: 0,
  y: 0,
  z: 1,
}

export const time = {
  start: performance.now(),
}

export function reset() {
  time.start = performance.now()
  mousePosition.x = mousePosition.y = 0
  scrollPosition.x = scrollPosition.y = 1
  cameraPosition.x = cameraPosition.y = cameraPosition.z = 0
  cameraRotation.x = cameraRotation.y = cameraRotation.z = 0
  cameraDir.x = cameraDir.y = 0
  cameraDir.z = 1
}

export function update() {
  // cameraRotation.z = 0
  cameraDir.x = Math.sin(cameraRotation.y) * Math.cos(cameraRotation.x)
  cameraDir.y = -Math.sin(cameraRotation.x)
  cameraDir.z = Math.cos(cameraRotation.y) * Math.cos(cameraRotation.x)

  // pitch pi/2
  const perp = {
    x: cameraDir.z,
    y: cameraDir.y,
    z: -cameraDir.x,
  }

  let speed = movementSpeed

  if (keyState['Shift']) {
    speed *= 10
    // movementSpeed *= 1.1
  }
  // else {
  //   movementSpeed = 0.1
  // }

  if (keyState['w']) {
    cameraPosition.x += cameraDir.x * speed
    cameraPosition.y += cameraDir.y * speed
    cameraPosition.z += cameraDir.z * speed
  }

  if (keyState['s']) {
    cameraPosition.x -= cameraDir.x * speed
    cameraPosition.y -= cameraDir.y * speed
    cameraPosition.z -= cameraDir.z * speed
  }

  if (keyState['a']) {
    cameraPosition.x -= perp.x * speed
    cameraPosition.z -= perp.z * speed
  }

  if (keyState['d']) {
    cameraPosition.x += perp.x * speed
    cameraPosition.z += perp.z * speed
  }
}
