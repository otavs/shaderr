import { atomWithImmer } from 'jotai/immer'
import { Apps } from 'App/Apps'
import { Uniforms } from 'App/types'

const defaultApp = 'Sierpinski'

export const appNameAtom = atomWithImmer(
  Apps[defaultApp] ? defaultApp : Object.keys(Apps)[0]
)

export const uniformsAtom = atomWithImmer<Uniforms>({})
