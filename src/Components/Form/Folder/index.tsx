import { useState, useEffect, useRef } from 'react'
import { Arrow, Content, HeaderDiv, Main } from './styles'

type HeaderProps = {
  isOpen: boolean
  onClick: () => void
}

const Header = ({ isOpen, onClick }: HeaderProps) => {
  return (
    <HeaderDiv
      isOpen={isOpen}
      onClick={onClick}
      onMouseDown={(e) => e.detail > 1 && e.preventDefault()}
    >
      <Arrow rotated={isOpen} />
      Test
      {/* <span style={{position: 'absolute', right: 0}}> {'>'} </span> */}
    </HeaderDiv>
  )
}

type Props = {
  children?: React.ReactNode
  isOpenDefault?: boolean
}

export default ({ children, isOpenDefault = false }: Props) => {
  const [isOpen, setIsOpen] = useState(isOpenDefault)

  const toggle = () => setIsOpen((isOpen) => !isOpen)

  return (
    <Main>
      <Header isOpen={isOpen} onClick={toggle} />
      <Content hidden={!isOpen}>{children}</Content>
    </Main>
  )
}
