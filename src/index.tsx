import React from 'react'
import ReactDOM from 'react-dom/client'
import Main from 'Components/Main'
import './index.css'
import { loadApps } from 'App/Apps'

loadApps().then(() => {
  const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement)
  root.render(
    // <React.StrictMode>
    <Main />
    // </React.StrictMode>
  )  
})