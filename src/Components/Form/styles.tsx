import styled from 'styled-components'

export const FormParams = styled.div`
  /* background-color: green; */
  padding: 6px;
`

export const SelectorDiv = styled.div`
  /* background-color: #0000ff; */
`

export const InputDiv = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`

export const Input = styled.input<{ width: number }>`
  border: 0;
  background-color: transparent;
  font-size: 16px;
  width: ${({ width }) => `${width || 1}ch`};

  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  -moz-appearance: textfield;
`
