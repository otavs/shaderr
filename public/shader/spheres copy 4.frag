#version 300 es

precision highp float;

uniform vec2 u_resolution;
uniform float u_time;
uniform vec2 u_mouse;
uniform float u_scroll;
uniform vec3 u_position;
uniform vec3 u_rotation;
uniform vec3 u_direction;

out vec4 fragColor;

uniform int maxRayTraceSteps;
uniform float maxRayTraceDist;
uniform float minRayTraceSurfaceDist;

vec3 hsv2rgb(vec3 c) {
  vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
  vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
  return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float sdSphere(vec3 p, vec3 center, float radius) {
  return length(center - mod(2.*p, 4.0)) - radius;
  // return length(sin(center) - sin(p)) - radius;
}

float getDist(vec3 p) {
  float dist = maxRayTraceDist;

  dist = min(dist, sdSphere(p, vec3(0, 0, 4), 1.));

  return dist;
}

float rayMarch(vec3 origin, vec3 dir) {
  float totalDist = 0.;
  for (int i = 0; i < maxRayTraceSteps; i++) {
    vec3 p = origin + totalDist * dir;
    float dist = getDist(p);
    totalDist += dist;
    if (totalDist > maxRayTraceDist || dist < minRayTraceSurfaceDist)
      break;
  }
  return totalDist;
}

vec3 getNormal(vec3 p) {
  float dist = getDist(p);
  vec2 e = vec2(.01, 0);
  vec3 normal =
      dist - vec3(getDist(p - e.xyy), getDist(p - e.yxy), getDist(p - e.yyx));
  return normalize(normal);
}

float getLight(vec3 p) {
  // vec3 lightPos = vec3(0, 4, 4);
  vec3 lightPos = u_position;
  // lightPos.xz += vec2(sin(u_time), cos(u_time)) * 2.;
  vec3 pToLight = normalize(lightPos - p);
  vec3 normal = getNormal(p);
  float light = clamp(dot(normal, pToLight), 0., 1.);
  float q = rayMarch(p + 2. * minRayTraceSurfaceDist * normal, pToLight);
  if (q < length(lightPos - p)) {
    light *= .1;
  }
  return light;
}

vec3 rotateCamera(vec3 dir) {
  float a_x = -u_rotation.x;
  float a_y = -u_rotation.y;
  float a_z = 0.;

  mat3 Rz = mat3(cos(a_z), -sin(a_z), 0, sin(a_z), cos(a_z), 0, 0, 0, 1);
  mat3 Ry = mat3(cos(a_y), 0, sin(a_y), 0, 1, 0, -sin(a_y), 0, cos(a_y));
  mat3 Rx = mat3(1, 0, 0, 0, cos(a_x), -sin(a_x), 0, sin(a_x), cos(a_x));

  return Rz * Ry * Rx * dir;
}

void main() {
  vec2 uv = (2.0 * gl_FragCoord.xy - u_resolution.xy) / u_resolution.y;

  vec3 dir = normalize(vec3(uv, 2));
  dir = rotateCamera(dir);

  float dist = rayMarch(u_position, dir);
  vec3 p = u_position + dist * dir;
  float difuseLight = getLight(p);

  vec3 col = vec3(difuseLight);
  // vec3 col = hsv2rgb(vec3(dist, 1, 1));
  // vec3 col = getNormal(p);
  fragColor = vec4(col, 1.0);
}